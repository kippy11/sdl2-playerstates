#include "Player.h"
#include "TextureUtils.h"

/**
 * initPlayer
 * 
 * Function to populate an animation structure from given paramters. 
 * 
 * @param player Player structure to populate 
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @return void?  Could do with returning a value for success/error 
 */
void initPlayer(Player* player, SDL_Renderer *renderer)
{

    // Create player texture from file, optimised for renderer 
    // There are better ways of doing this, i.e. a Resource Manager 
    // This will do for now :-D
    player->texture = createTextureFromFile("assets/images/undeadking.png", renderer);

    // Allocate memory for the animation structures
    for (int i = 0; i < player->MAX_ANIMATIONS; i++)
    {
       player->animations[i] = new Animation;
    }
    
    // Setup the animation structure
    initAnimation(player->animations[LEFT], 3, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 1);
    initAnimation(player->animations[RIGHT], 3, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 2);
    initAnimation(player->animations[UP], 3, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 3);
    initAnimation(player->animations[DOWN], 3, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 0);
    initAnimation(player->animations[IDLE], 1, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 0);

    // set the default state to idle 
    player->state = IDLE;

    // Target is the same size of the source
    // We could choose any size - experiment with this :-D
    player->targetRectangle.w = player->SPRITE_WIDTH;
    player->targetRectangle.h = player->SPRITE_HEIGHT;
}

/**
 * destPlayer
 * 
 * Function to clean up an player structure.  
 * 
 * @param player Player structure to destroy
 */
void destPlayer(Player* player)
{
    // Clean up animations - free memory
    for (int i = 0; i < player->MAX_ANIMATIONS; i++)
    {
        destAnimation(player->animations[i]);

        // Clean up the animaton structure
        // allocated with new so use delete. 
        delete player->animations[i];
        player->animations[i] = nullptr;
    }

    // Clean up 
    SDL_DestroyTexture(player->texture);
    player->texture = nullptr; 
}

/**
 * drawPlayer
 * 
 * Function draw a Player structure
 * 
 * @param player Player structure tp draw
 * @param renderer SDL_Renderer to draw to
 */
void drawPlayer(Player* player, SDL_Renderer *renderer)
{
    // Get current animation (only have one)!
    Animation* current = player->animations[player->state];

    SDL_RenderCopy(renderer, player->texture, &current->frames[current->currentFrame], &player->targetRectangle);
}

/**
 * processInput
 * 
 * Function to process inputs for the player structure
 * Note: Need to think about other forms of input!
 * 
 * @param player Player structure processing the input
 * @param keyStates The keystates array. 
 */
void processInput(Player *player, const Uint8 *keyStates)
{
    // Process Player Input
        
    //Input - keys/joysticks?
    float verticalInput = 0.0f; 
    float horizontalInput = 0.0f; 

    // If no keys are down player should not animate!
    player->state = IDLE;

    // This could be more complex, e.g. increasing the vertical 
    // input while the key is held down. 
    if (keyStates[SDL_SCANCODE_UP]) 
    {
        verticalInput = -1.0f;
        player->state = UP;
    }

    if (keyStates[SDL_SCANCODE_DOWN]) 
    {
        verticalInput = 1.0f;
        player->state = DOWN;
    }
  
    if (keyStates[SDL_SCANCODE_RIGHT]) 
    {
        horizontalInput = 1.0f;
        player->state = RIGHT;
    }

    if (keyStates[SDL_SCANCODE_LEFT]) 
    {
        horizontalInput = -1.0f;
        player->state = LEFT;
    }

    // Calculate player velocity. 
    // Note: This is imperfect, no account taken of diagonal!
    player->vy = verticalInput * player->speed;
    player->vx = horizontalInput * player->speed;
}

/**
 * updatePlayer
 * 
 * Function to update the player structure and its components
 * 
 * @param player Player structure being updated
 * @param timeDeltaInSeconds the time delta in seconds
 */
void updatePlayer(Player* player, float timeDeltaInSeconds)
{
    // Calculate distance travelled since last update
    float yMovement = timeDeltaInSeconds * player->vy;
    float xMovement = timeDeltaInSeconds * player->vx;

    // Update player position.
    player->x += xMovement;
    player->y += yMovement;

    // Move sprite to nearest pixel location.
    player->targetRectangle.y = round(player->y);
    player->targetRectangle.x = round(player->x);

    // Get current animation
    Animation* current = player->animations[player->state];

    // Add elapsed time to the animation accumulator. 
    current->accumulator += timeDeltaInSeconds;

    // Check if animation needs update
    if(current->accumulator > 0.4f) //25fps?
    {
        current->currentFrame++;
        current->accumulator = 0.0f;

        if(current->currentFrame >= current->maxFrames)
        {
            current->currentFrame = 0;
        }
    }
}